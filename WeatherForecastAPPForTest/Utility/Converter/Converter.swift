//
//  Converter.swift
//  WeatherForecastAPPForTest
//
//  Created by IT-EFW-65-03 on 18/6/2566 BE.
//

import Foundation

class Converter : NSObject {
    static func calculateCelsius(fahrenheit: Double) -> Double {
        var celsius: Double
        
        celsius = (fahrenheit - 32) * 5 / 9
        
        return celsius
    }
    
    static func calculateFahrenheit(celsius: Double) -> Double {
        var fahrenheit: Double
        
        fahrenheit = celsius * 9 / 5 + 32
        
        return fahrenheit
    }
    
    static func unixTimeToDateDisplay(unixTime : Double) -> String {
        let date = Date(timeIntervalSince1970: unixTime)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")

        dateFormatter.dateFormat = "E, d/MM" // OR "dd-MM-yyyy"

        let currentDateString: String = dateFormatter.string(from: date)
        return currentDateString
    }
    
    static func unixTimeToDateString(unixTime : Double) -> String {
        let date = Date(timeIntervalSince1970: unixTime)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")

        dateFormatter.dateFormat = "yyyy-MM-dd" // OR "dd-MM-yyyy"

        let currentDateString: String = dateFormatter.string(from: date)
        return currentDateString
    }
}
