//
//  ApiModel.swift
//  WeatherForecastAPPForTest
//
//  Created by IT-EFW-65-03 on 17/6/2566 BE.
//

import Foundation

struct GetRequestModel : Codable {
    var key : String = ""
    var value : String = ""
}
