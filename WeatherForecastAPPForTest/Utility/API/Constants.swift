//
//  Constants.swift
//  demoFirebase
//
//  Created by IT-EFW-65-03 on 3/11/2565 BE.
//

import Foundation
import UIKit

struct Constants {
    static let MAX_DAYS = 5
    static let unit = "°C"
    static let itle_bt_convert = "Convert To °F"
    static let currentUint: TempUnit = TempUnit.celsius

    //The API's base URL
    static let baseUrl = "https://api.openweathermap.org/data/2.5/"
    static let baseImageUrl = "https://openweathermap.org/img/w/"
    static let API_KEY = "17234b554a9d5b27f2e466f02464e39b"
    
    //The header fields
    enum HttpHeaderField: String {
        case contentType = "Content-Type"
        case acceptType = "Accept"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json
        
        var rawValue: String {
            get {
                switch self {
                case .json:
                    return "application/json"
                }
            }
        }
    }
}

enum TempUnit {
    case celsius, fahrenheit
}
