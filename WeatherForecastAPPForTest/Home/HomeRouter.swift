//
//  HomeRouter.swift
//  WeatherForecastAPPForTest
//
//  Created by IT-EFW-65-03 on 17/6/2566 BE.
//  Copyright (c) 2566 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol HomeRoutingLogic
{
    func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol HomeDataPassing
{
    var dataStore: HomeDataStore? { get }
}

class HomeRouter: NSObject, HomeRoutingLogic, HomeDataPassing
{
    weak var viewController: HomeViewController?
    var dataStore: HomeDataStore?
    
    // MARK: Routing
    
    func routeToSomewhere(segue: UIStoryboardSegue?)
    {
        if dataStore?.city == "" {
            self.viewController?.displayCityNotFound("Please Enter City.")
            return
        }
        
        if let segue = segue {
            let destinationVC = segue.destination as! FiveDaysForecastViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let destinationVC = storyboard.instantiateViewController(withIdentifier: "FiveDaysForecastViewController") as! FiveDaysForecastViewController
            var destinationDS = destinationVC.router!.dataStore!
            passDataToSomewhere(source: dataStore!, destination: &destinationDS)
            navigateToSomewhere(source: viewController!, destination: destinationVC)
        }
    }
    
    // MARK: Navigation
    
    func navigateToSomewhere(source: HomeViewController, destination: FiveDaysForecastViewController)
    {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    // MARK: Passing data
    
    func passDataToSomewhere(source: HomeDataStore, destination: inout FiveDaysForecastDataStore)
    {
        destination.city = source.city
    }
}
