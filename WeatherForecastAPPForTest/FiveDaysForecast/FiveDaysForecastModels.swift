//
//  FiveDaysForecastModels.swift
//  WeatherForecastAPPForTest
//
//  Created by IT-EFW-65-03 on 18/6/2566 BE.
//  Copyright (c) 2566 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum FiveDaysForecast
{
  // MARK: Use cases
  
  enum Something
  {
      struct Request
      {
          var city : String?
      }
      struct Response
      {
          var item : [List]?
          var city : String
          var title_bt_convert : String
          var unit : String
      }
      struct ViewModel
      {
          var item : [List]?
          var city : String
          var title_bt_convert : String
          var unit : String
      }
  }
}
