//
//  FiveDaysForecastTableViewCell.swift
//  WeatherForecastAPPForTest
//
//  Created by IT-EFW-65-03 on 18/6/2566 BE.
//

import UIKit

class FiveDaysForecastTableViewCell: UITableViewCell {
    
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lb_date: UILabel!
    @IBOutlet weak var lb_temp: UILabel!
    @IBOutlet weak var lb_humidity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        view_bg.layer.cornerRadius = 13
        
        view_bg.layer.shadowColor = UIColor.lightText.cgColor
        view_bg.layer.shadowOffset = CGSize(width: 0.0, height: 5)
        view_bg.layer.shadowOpacity = 1.0
        view_bg.layer.shadowRadius = 5.0
        view_bg.layer.masksToBounds = false
        view_bg.layer.cornerRadius = 18
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ item:List, units: String){
        lb_date.text = Converter.unixTimeToDateDisplay(unixTime: Double(item.dt))
        lb_temp.text =  String.init(format: "%@ %@", item.main.temp.removeZerosFromEnd() , units)
        lb_humidity.text = String(item.main.humidity)
        
        let icon = item.weather.first?.icon ?? ""
        let url = URL(string: String.init(format: "%@%@%@", Constants.baseImageUrl , icon , ".png"))
        img_icon.sd_setImage(with: url, placeholderImage: UIImage(systemName: "questionmark"))
    }
    
}
